'use strict'
require('./check-versions')()

process.env.NODE_ENV = 'production'

const ora = require('ora')
const rm = require('rimraf')
const path = require('path')
const chalk = require('chalk')
const shell = require('shelljs')
const webpack = require('webpack')
const config = require('../config')
const webpackConfig = require('./webpack.prod-underline.conf')
const fs = require('fs')

const spinner = ora('building for production...')
spinner.start()

var assetsPath = path.join(config.build.assetsRoot, config.build.assetsSubDirectory)
shell.rm('-rf', assetsPath)
shell.mkdir('-p', assetsPath)
shell.config.silent = true
shell.cp('-R', 'static/*', assetsPath)
shell.config.silent = false

rm(path.join(config.build.assetsRoot, config.build.assetsSubDirectory), err => {
  if (err) throw err
  webpack(webpackConfig, (err, stats) => {
    spinner.stop()
    if (err) throw err
    process.stdout.write(stats.toString({
      colors: true,
      modules: false,
      children: false, // If you are using ts-loader, setting this to true will make TypeScript errors show up during build.
      chunks: false,
      chunkModules: false
    }) + '\n\n')

    if (stats.hasErrors()) {
      console.log(chalk.red('  Build failed with errors.\n'))
      process.exit(1)
    }

    // 生产环境才生成
    if (process.env.NODE_ENV === 'production') {
      storeTargetPath()
    }

    console.log(chalk.cyan('  Build complete.\n'))
    console.log(chalk.yellow(
      '  Tip: built files are meant to be served over an HTTP server.\n' +
      '  Opening index.html over file:// won\'t work.\n'
    ))
  })
})

// 将对应的 js、css、img 等文件放到正式目录下
function storeTargetPath() {
  var targetRoot = config.build.targetRoot,
      buildPath = assetsPath,
      targetStatic = path.join(targetRoot, config.build.assetsSubDirectory);

  // 清理静态文件
  shell.rm('-rf', targetStatic)

  // 复制对应的文件到正式目录
  shell.cp('-R', buildPath, targetRoot)

  var jsPath = shell.ls(targetStatic + '/js/*.js').map((file) => path.relative(targetRoot, file)),
      cssPath = shell.ls(targetStatic + '/css/*.css').map((file) => path.relative(targetRoot, file)),
      sortJsPath = [];
    
  /*
  jsPath = [ 'static/js/app.29fe04dd5983ad5b65f5.js',
   'static/js/manifest.6142e1310dee54769ddb.js',
   'static/js/vendor.66d11e8a6c18e664cebb.js'  ]
  */

  // 排序，js 需要依赖顺序
  sortJsPath.push(jsPath.filter((file) => file.indexOf('manifest.') !== -1)[0])
  sortJsPath.push(jsPath.filter((file) => file.indexOf('vendor.') !== -1)[0])
  sortJsPath.push(jsPath.filter((file) => file.indexOf('app.') !== -1)[0])

  fs.writeFileSync(
    path.join(targetRoot, 'style.json'), 
    JSON.stringify({
      'js' : sortJsPath,
      'css' : cssPath,
    })
  )
}

