import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
    mode: 'history',
    base: '/yueyeweb/',
    routes: [
        {
            path: '/',
            redirect: '/login'
        },
        {
            path: '/',
            component: resolve => require(['../views/layout/Home.vue'], resolve),
            children: [
                // 欢迎页面
                {
                    path: '/',
                    name: 'readme',
                    meta: {
                        title: '欢迎公告',
                        breadcrumb: false, // 面包屑
                        keepAlive: false // 需要缓存
                    },
                    component: resolve => require(['../views/layout/Readme.vue'], resolve)
                },
                // 用户列表
                {
                    path: 'user',
                    component: resolve => require(['../views/user/index.vue'], resolve),
                    children: [
                        {
                            path: 'man',
                            name: 'user.man',
                            meta: {
                                title: '男用户列表',
                                breadcrumb: false, // 面包屑
                                keepAlive: true // 需要缓存
                            },
                            component: resolve => require(['../views/user/man.vue'], resolve)
                        },
                        {
                            path: 'woman',
                            name: 'user.woman',
                            meta: {
                                title: '女用户列表',
                                breadcrumb: false, // 面包屑
                                keepAlive: true // 需要缓存
                            },
                            component: resolve => require(['../views/user/woman.vue'], resolve)
                        },
                        {
                            path: 'identity',
                            name: 'user.identity',
                            meta: {
                                title: '女性认证列表',
                                breadcrumb: false, // 面包屑
                                keepAlive: true // 需要缓存
                            },
                            component: resolve => require(['../views/user/identity.vue'], resolve)
                        },
                        {
                            path: 'devicecode',
                            name: 'user.devicecode',
                            meta: {
                                title: '用户设备码列表',
                                breadcrumb: false,
                                keepAlive: false
                            },
                            component: resolve => require(['../views/user/devicecode.vue'], resolve)
                        },
                        {
                            path: 'mandel',
                            name: 'user.mandel',
                            meta: {
                                title: '男用户详情',
                                breadcrumb: false, // 面包屑
                                keepAlive: false // 需要缓存
                            },
                            component: resolve => require(['../views/user/mandel.vue'], resolve)
                        },
                        {
                            path: 'womandel',
                            name: 'user.womandel',
                            meta: {
                                title: '女用户详情',
                                breadcrumb: false, // 面包屑
                                keepAlive: false // 需要缓存
                            },
                            component: resolve => require(['../views/user/womandel.vue'], resolve)
                        }
                        // {
                        //     path: 'password',
                        //     name: 'user.password',
                        //     meta: {
                        //         title: '重置密码',
                        //         breadcrumb: false, // 面包屑
                        //         keepAlive: false // 需要缓存
                        //     },
                        //     component: resolve => require(['../views/user/password.vue'], resolve)
                        // }
                    ]
                },
                // 邀请码
                {
                    path: 'code',
                    component: resolve => require(['../views/code/index.vue'], resolve),
                    children: [
                        {
                            path: 'new',
                            name: 'code.new',
                            meta: {
                                title: '新用户申请',
                                breadcrumb: false, // 面包屑
                                keepAlive: false // 缓存
                            },
                            component: resolve => require(['../views/code/new.vue'], resolve)
                        },
                        {
                            path: 'old',
                            name: 'code.old',
                            meta: {
                                title: '旧用户申请',
                                breadcrumb: false,
                                keepAlive: false
                            },
                            component: resolve => require(['../views/code/old.vue'], resolve)
                        }
                    ]
                },
                {
                    path: 'detail',
                    component: resolve => require(['../views/detail/index.vue'], resolve),
                    children: [
                        {
                            path: 'man',
                            name: 'detail.man',
                            meta: {
                                title: '男用户列表',
                                breadcrumb: false, // 面包屑
                                keepAlive: false // 需要缓存
                            },
                            component: resolve => require(['../views/detail/man.vue'], resolve)
                        },
                        {
                            path: 'woman',
                            name: 'detail.woman',
                            meta: {
                                title: '女用户列表',
                                breadcrumb: false, // 面包屑
                                keepAlive: false // 需要缓存
                            },
                            component: resolve => require(['../views/detail/woman.vue'], resolve)
                        }
                    ]
                },
                {
                    path: 'broadCast',
                    component: resolve => require(['../views/broadCast/index.vue'], resolve),
                    children: [
                        {
                            path: 'man',
                            name: 'broadCast.man',
                            meta: {
                                title: '男士广播',
                                breadcrumb: false, // 面包屑
                                keepAlive: true // 需要缓存
                            },
                            component: resolve => require(['../views/broadCast/man.vue'], resolve)
                        },
                        {
                            path: 'woman',
                            name: 'broadCast.woman',
                            meta: {
                                title: '女士广播',
                                breadcrumb: false, // 面包屑
                                keepAlive: true // 需要缓存
                            },
                            component: resolve => require(['../views/broadCast/woman.vue'], resolve)
                        },
                        {
                            path: 'detail',
                            name: 'broadCast.detail',
                            meta: {
                                title: '广播详情',
                                breadcrumb: false, // 面包屑
                                keepAlive: false // 需要缓存
                            },
                            component: resolve => require(['../views/broadCast/detail.vue'], resolve)
                        },
                        {
                            path: 'signName',
                            name: 'broadCast.signName',
                            meta: {
                                title: '报名列表',
                                breadcrumb: false, // 面包屑
                                keepAlive: false // 需要缓存
                            },
                            component: resolve => require(['../views/broadCast/signName.vue'], resolve)
                        }
                    ]
                },
                {
                    path: 'report',
                    component: resolve => require(['../views/report/index.vue'], resolve),
                    children: [
                        {
                            path: 'man',
                            name: 'report.man',
                            meta: {
                                title: '男士举报',
                                breadcrumb: false, // 面包屑
                                keepAlive: false // 需要缓存
                            },
                            component: resolve => require(['../views/report/man.vue'], resolve)
                        },
                        {
                            path: 'woman',
                            name: 'report.woman',
                            meta: {
                                title: '女士举报',
                                breadcrumb: false, // 面包屑
                                keepAlive: false // 需要缓存
                            },
                            component: resolve => require(['../views/report/woman.vue'], resolve)
                        },
                        {
                            path: 'detail',
                            name: 'report.detail',
                            meta: {
                                title: '举报详情',
                                breadcrumb: false, // 面包屑
                                keepAlive: true // 需要缓存
                            },
                            component: resolve => require(['../views/report/detail.vue'], resolve)
                        }
                    ]
                },
                {
                    path: 'evaluate',
                    component: resolve => require(['../views/evaluate/index.vue'], resolve),
                    children: [
                        {
                            path: 'man',
                            name: 'evaluate.man',
                            meta: {
                                title: '男士评价',
                                breadcrumb: false, // 面包屑
                                keepAlive: true // 需要缓存
                            },
                            component: resolve => require(['../views/evaluate/man.vue'], resolve)
                        },
                        {
                            path: 'woman',
                            name: 'evaluate.woman',
                            meta: {
                                title: '女士评价',
                                breadcrumb: false, // 面包屑
                                keepAlive: true // 需要缓存
                            },
                            component: resolve => require(['../views/evaluate/woman.vue'], resolve)
                        }
                    ]
                },
                {
                    path: 'identification',
                    component: resolve => require(['../views/report/index.vue'], resolve),
                    children: [
                        {
                            path: 'list',
                            name: 'identification.list',
                            meta: {
                                title: '用户认证',
                                breadcrumb: false, // 面包屑
                                keepAlive: true // 需要缓存
                            },
                            component: resolve => require(['../views/identification/list.vue'], resolve)
                        }
                    ]
                },
                {
                    path: 'withdrawCash',
                    component: resolve => require(['../views/withdrawCash/index.vue'], resolve),
                    children: [
                        {
                            path: 'unhandle',
                            name: 'withdrawCash.unhandle',
                            meta: {
                                title: '待处理提现',
                                breadcrumb: false, // 面包屑
                                keepAlive: true // 需要缓存
                            },
                            component: resolve => require(['../views/withdrawCash/unhandle.vue'], resolve)
                        },
                        {
                            path: 'handle',
                            name: 'withdrawCash.handle',
                            meta: {
                                title: '已处理提现',
                                breadcrumb: false, // 面包屑
                                keepAlive: true // 需要缓存
                            },
                            component: resolve => require(['../views/withdrawCash/handle.vue'], resolve)
                        },
                        {
                            path: 'history',
                            name: 'withdrawCash.history',
                            meta: {
                                title: '历史明细',
                                breadcrumb: false, // 面包屑
                                keepAlive: false // 需要缓存
                            },
                            component: resolve => require(['../views/withdrawCash/history.vue'], resolve)
                        }
                    ]
                },
                {
                    path: 'deposit',
                    component: resolve => require(['../views/deposit/index.vue'], resolve),
                    children: [
                        {
                            path: 'deposit',
                            name: 'deposit.deposit',
                            meta: {
                                title: '定金列表',
                                breadcrumb: false, // 面包屑
                                keepAlive: false // 需要缓存
                            },
                            component: resolve => require(['../views/deposit/deposit.vue'], resolve)
                        },
                        {
                            path: 'refund',
                            name: 'deposit.refund',
                            meta: {
                                title: '退款待制裁',
                                breadcrumb: false, // 面包屑
                                keepAlive: false // 需要缓存
                            },
                            component: resolve => require(['../views/deposit/refund.vue'], resolve)
                        },
                        {
                            path: 'sanction',
                            name: 'deposit.sanction',
                            meta: {
                                title: '制裁历史',
                                breadcrumb: false, // 面包屑
                                keepAlive: false // 需要缓存
                            },
                            component: resolve => require(['../views/deposit/sanction.vue'], resolve)
                        },
                        {
                            path: 'transferAccounts',
                            name: 'deposit.transferAccounts',
                            meta: {
                                title: '转账历史',
                                breadcrumb: false, // 面包屑
                                keepAlive: false // 需要缓存
                            },
                            component: resolve => require(['../views/deposit/transferAccounts.vue'], resolve)
                        }
                    ]
                },
                {
                    path: 'system',
                    component: resolve => require(['../views/system/index.vue'], resolve),
                    children: [
                        {
                            path: 'system',
                            name: 'system.system',
                            meta: {
                                title: '系统参数',
                                breadcrumb: false, // 面包屑
                                keepAlive: false // 需要缓存
                            },
                            component: resolve => require(['../views/system/system.vue'], resolve)
                        },
                        {
                            path: 'member',
                            name: 'system.member',
                            meta: {
                                title: '会员套餐',
                                breadcrumb: false, // 面包屑
                                keepAlive: false // 需要缓存
                            },
                            component: resolve => require(['../views/system/member.vue'], resolve)
                        },
                        {
                            path: 'message',
                            name: 'system.message',
                            meta: {
                                title: '系统消息',
                                breadcrumb: false, // 面包屑
                                keepAlive: false // 需要缓存
                            },
                            component: resolve => require(['../views/system/message.vue'], resolve)
                        },
                        {
                            path: 'income',
                            name: 'system.income',
                            meta: {
                                title: '收入记录',
                                breadcrumb: false, // 面包屑
                                keepAlive: false // 需要缓存
                            },
                            component: resolve => require(['../views/system/income.vue'], resolve)
                        },
                        {
                            path: 'detail',
                            name: 'system.detail',
                            meta: {
                                title: '收入明细',
                                breadcrumb: false, // 面包屑
                                keepAlive: false // 需要缓存
                            },
                            component: resolve => require(['../views/system/detail.vue'], resolve)
                        },
                        {
                            path: 'worker',
                            name: 'system.worker',
                            meta: {
                                title: '客服列表',
                                breadcrumb: false, // 面包屑
                                keepAlive: false // 需要缓存
                            },
                            component: resolve => require(['../views/system/worker.vue'], resolve)
                        },
                        {
                            path: 'sourseWorker',
                            name: 'system.sourseWorker',
                            meta: {
                                title: '客服详情',
                                breadcrumb: false, // 面包屑
                                keepAlive: false // 需要缓存
                            },
                            component: resolve => require(['../views/system/sourseWorker.vue'], resolve)
                        }
                    ]
                }
            ]
        },
        {
            path: '/login',
            name: 'Login',
            component: resolve => require(['../views/login/index.vue'], resolve)
        }
    ]
})
