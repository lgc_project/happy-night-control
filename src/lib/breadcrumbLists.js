// 面包屑
const items = {
    'monitorSet': {
        'monitorSet.site': [
            {
                to: {},
                text: '站点管理'
            }
        ],
        'monitorSet.site.sourceAdd': [
            {
                to: {
                    name: 'monitorSet.site'
                },
                text: '站点管理'
            },
            {
                to: {},
                text: '新增站点'
            }
        ],
        'monitorSet.site.sourceEdit': [
            {
                to: {
                    name: 'monitorSet.site'
                },
                text: '站点管理'
            },
            {
                to: {},
                text: '站点详情编辑'
            }
        ],
        'monitorSet.keyword': [
            {
                to: {},
                text: '关键字管理'
            }
        ],
        'monitorSet.keyword.sourceAdd': [
            {
                to: {
                    name: 'monitorSet.keyword'
                },
                text: '关键字管理'
            },
            {
                to: {},
                text: '新增关键字'
            }
        ],
        'monitorSet.keyword.sourceEdit': [
            {
                to: {
                    name: 'monitorSet.keyword'
                },
                text: '关键字管理'
            },
            {
                to: {},
                text: '关键字详情编辑'
            }
        ],
        'monitorSet.notifier': [
            {
                to: {},
                text: '通知管理'
            }
        ],
        'monitorSet.notifier.sourceAdd': [
            {
                to: {
                    name: 'monitorSet.notifier'
                },
                text: '通知组管理'
            },
            {
                to: {},
                text: '新增通知组'
            }
        ],
        'monitorSet.notifier.sourceEdit': [
            {
                to: {
                    name: 'monitorSet.notifier'
                },
                text: '通知组管理'
            },
            {
                to: {},
                text: '通知组详情编辑'
            }
        ]
    },
    'monitorData': {
        'monitorData.dataList': [
            {
                to: {},
                text: '监测数据'
            }
        ],
        'monitorData.statistic': [
            {
                to: {},
                text: '数据统计表'
            }
        ]
    }
}
export default items
