import axios from 'axios'
import apiList from './resource.js'

export default {
    // 获取配置
    getConfig: function (successfn, errorfn) {
        return axios.get(apiList.common.config)
            .then(res => {
                successfn && successfn(res.data)
            })
            .catch(res => {
                errorfn && errorfn(res.data)
            })
    }
}
