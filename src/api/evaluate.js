import axios from 'axios'
import apiList from './resource.js'

export default {
    getEvaluateList: function (data, successfn, errorfn) {
        return axios.get(apiList.evaluate.list, data)
            .then(res => {
                successfn && successfn(res.data)
            })
            .catch(res => {
                errorfn && errorfn(res.data)
            })
    }
}
