// 接口前缀
const apiPrefix = window.basicData.apiPrefix
// 用于存放处理后的接口地址
const modules = {}
// 普通api
const api = {
    common: {
        config: '/api/v1/all-config' // 获取所有配置
    },
    account: {
        login: '/yueye/sys/login', // 登录
        logout: '/api/admin/logout', // 退出登录
        info: '/api/admin/login-info' // 获取登录信息
    },
    user: {
        userList: '/yueye/user/list', // 获取用户列表
        deviceCodeList: '/yueye/user/deviceCodeList', // 获取设备码列表
        editDeviceCodeUrl: '/yueye/user/editDeviceCode', // 封禁/解封设备
        identityList: '/yueye/user/identityList', // 认证列表
        editIdentityUrl: '/yueye/user/editIdentity', // 是否通过认证
        consumeListUrl: '/yueye/pay/payLogList', // 历史消费
        incomeListUrl: '/yueye/pay/incomeList', // 收入记录
        userDetailUrl: '/yueye/user/userDetail', // 用户详情
        userDelUrl: '/yueye/user/userDel', // 用户删除
        userFreezeOrThawUrl: '/yueye/user/userFreezeOrThaw', // 用户冻结/解冻
        editPhotoStatusUrl: '/yueye/userPhoto/editPhotoStatus' // 封禁/解封图片
    },
    broadCast: {
        list: '/yueye/broadCast/findListBySex', // 根据性别获取广播列表
        detail: '/yueye/broadCast/findById', // 根据id获取广播详情
        forbid: '/yueye/broadCast/forbidImages', // 封禁图片
        signName: '/yueye/broadCast/findSignerList', // 根据id获取广播报名人数
        del: '/yueye/broadCast/deleteByAdmin' // 根据性别获取广播列表
    },
    code: {
        codeList: '/yueye/inviteCode/list', // 获取邀请码列表
        buildCodeUrl: '/yueye/inviteCode/buildCode' // 生成邀请码
    },
    withdraw: {
        list: '/yueye/userWithdraw/findListByStatus', // 获取提现列表
        handle: '/yueye/userWithdraw/dealUserWithdraw', // 提现->执行/打回
        detail: '/yueye/userWithdraw/findListByStatus' // 获取提现明细
    },
    evaluate: {
        list: '/yueye/relationship/evaluatePage' // 获取评价列表
    },
    report: {
        list: '/yueye/user/findUserReportList' // 举报列表(包含详情)
    },
    system: {
        getData: '/yueye/sys/config/list', // 系统参数列表
        editData: '/yueye/sys/config/update', // 更新系统参数
        sendMsg: '/yueye/message/sendSysMsgToAll', // 发送系统消息
        rechargeListUrl: '/yueye/member/rechargeTypeList', // 获取会员套餐
        editRechargeUrl: '/yueye/member/editRechargeType', // 更新会员套餐
        getWorkerList: '', // 获取客服列表
        addWorker: '', // 新增客服
        editWorker: '', // 编辑客服
        getIncomeList: '/yueye/payLog/statistic', // 收入记录
        getIncomeDetail: '/yueye/payLog/statisticByCity', // 收入明细
        sysUserListUrl: '/yueye/sys/user/list' // 系统用户列表
    }
}
// 添加前缀函数
const addUrlPrefix = (module, obj, prefix) => {
    for (let key in obj) {
        let value = obj[key]
        if (typeof value === 'object') {
            if (modules[key] === undefined) {
                modules[key] = {}
            }
            addUrlPrefix(key, value, prefix)
        } else {
            if (module !== '' && value.indexOf('http') === -1) {
                modules[module][key] = prefix.concat(value)
            } else if (module === '' && value.indexOf('http') === -1) {
                modules[key] = prefix.concat(value)
            } else if (module !== '' && value.indexOf('http') !== -1) {
                modules[module][key] = value
            }
        }
    }
}
// 执行添加前缀
addUrlPrefix('', api, apiPrefix)

export default modules
