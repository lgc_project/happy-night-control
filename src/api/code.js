import axios from 'axios'
import apiList from './resource.js'

export default {
    getCodeList: function (data, successfn, errorfn) {
        return axios.get(apiList.code.codeList, data)
            .then(res => {
                successfn && successfn(res.data)
            })
            .catch(res => {
                errorfn && errorfn(res.data)
            })
    },
    buildCode: function (data, successfn, errorfn) {
        return axios.get(apiList.code.buildCodeUrl + '/' + data)
            .then(res => {
                successfn && successfn(res.data)
            })
            .catch(res => {
                errorfn && errorfn(res.data)
            })
    }
}
