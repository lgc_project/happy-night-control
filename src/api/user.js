import axios from 'axios'
import apiList from './resource.js'

export default {
    getUserList: function (data, successfn, errorfn) {
        return axios.get(apiList.user.userList, data)
            .then(res => {
                successfn && successfn(res.data)
            })
            .catch(res => {
                errorfn && errorfn(res.data)
            })
    },
    getDeviceCodeList: function (data, successfn, errorfn) {
        return axios.get(apiList.user.deviceCodeList, data)
            .then(res => {
                successfn && successfn(res.data)
            })
            .catch(res => {
                errorfn && errorfn(res.data)
            })
    },
    editDeviceCode: function (data, successfn, errorfn) {
        return axios.get(apiList.user.editDeviceCodeUrl + '/' + data.id + '/' + data.status)
            .then(res => {
                successfn && successfn(res.data)
            })
            .catch(res => {
                errorfn && errorfn(res.data)
            })
    },
    getIdentityList: function (data, successfn, errorfn) {
        return axios.get(apiList.user.identityList, data)
            .then(res => {
                successfn && successfn(res.data)
            })
            .catch(res => {
                errorfn && errorfn(res.data)
            })
    },
    editIdentity: function (data, successfn, errorfn) {
        return axios.get(apiList.user.editIdentityUrl + '/' + data.id + '/' + data.identity)
            .then(res => {
                successfn && successfn(res.data)
            })
            .catch(res => {
                errorfn && errorfn(res.data)
            })
    },
    consumeList: function (data, successfn, errorfn) {
        return axios.get(apiList.user.consumeListUrl, data)
            .then(res => {
                successfn && successfn(res.data)
            })
            .catch(res => {
                errorfn && errorfn(res.data)
            })
    },
    userDetail: function (data, successfn, errorfn) {
        return axios.get(apiList.user.userDetailUrl + '/' + data.id)
            .then(res => {
                successfn && successfn(res.data)
            })
            .catch(res => {
                errorfn && errorfn(res.data)
            })
    },
    userDel: function (data, successfn, errorfn) {
        return axios.get(apiList.user.userDelUrl + '/' + data.id)
            .then(res => {
                successfn && successfn(res.data)
            })
            .catch(res => {
                errorfn && errorfn(res.data)
            })
    },
    incomeList: function (data, successfn, errorfn) {
        return axios.get(apiList.user.incomeListUrl, data)
            .then(res => {
                successfn && successfn(res.data)
            })
            .catch(res => {
                errorfn && errorfn(res.data)
            })
    },
    userFreezeOrThaw: function (data, successfn, errorfn) {
        return axios.get(apiList.user.userFreezeOrThawUrl, data)
            .then(res => {
                successfn && successfn(res.data)
            })
            .catch(res => {
                errorfn && errorfn(res.data)
            })
    },
    editPhotoStatus: function (data, successfn, errorfn) {
        return axios.post(apiList.user.editPhotoStatusUrl + '/' + data.photoId + '/' + data.status)
            .then(res => {
                successfn && successfn(res.data)
            })
            .catch(res => {
                errorfn && errorfn(res.data)
            })
    }
}
