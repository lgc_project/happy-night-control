import axios from 'axios'
import apiList from './resource.js'

export default {
    getListBySex: function (sex, data, successfn, errorfn) {
        return axios.get(apiList.broadCast.list + '/' + sex, data)
            .then(res => {
                successfn && successfn(res.data)
            })
            .catch(res => {
                errorfn && errorfn(res.data)
            })
    },
    deleteBroadcast: function (userId, successfn, errorfn) {
        return axios.delete(apiList.broadCast.del + '/' + userId)
            .then(res => {
                successfn && successfn(res.data)
            })
            .catch(res => {
                errorfn && errorfn(res.data)
            })
    },
    forbidImages: function (data, successfn, errorfn) {
        return axios.get(apiList.broadCast.forbid, data)
            .then(res => {
                successfn && successfn(res.data)
            })
            .catch(res => {
                errorfn && errorfn(res.data)
            })
    },
    getDetail: function (userId, successfn, errorfn) {
        return axios.delete(apiList.broadCast.detail + '/' + userId)
            .then(res => {
                successfn && successfn(res.data)
            })
            .catch(res => {
                errorfn && errorfn(res.data)
            })
    },
    getSignName: function (data, successfn, errorfn) {
        return axios.get(apiList.broadCast.signName, data)
            .then(res => {
                successfn && successfn(res.data)
            })
            .catch(res => {
                errorfn && errorfn(res.data)
            })
    }
}
