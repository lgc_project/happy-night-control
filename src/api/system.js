import axios from 'axios'
import apiList from './resource.js'

export default {
    getData: function (successfn, errorfn) {
        return axios.get(apiList.system.getData)
            .then(res => {
                successfn && successfn(res.data)
            })
            .catch(res => {
                errorfn && errorfn(res.data)
            })
    },
    editData: function (data, successfn, errorfn) {
        return axios.get(apiList.system.editData, data)
            .then(res => {
                successfn && successfn(res.data)
            })
            .catch(res => {
                errorfn && errorfn(res.data)
            })
    },
    sendMsg: function (data, successfn, errorfn) {
        return axios.post(apiList.system.sendMsg + '?' + data)
            .then(res => {
                successfn && successfn(res.data)
            })
            .catch(res => {
                errorfn && errorfn(res.data)
            })
    },
    rechargeTypeList: function (data, successfn, errorfn) {
        return axios.post(apiList.system.rechargeListUrl, data)
            .then(res => {
                successfn && successfn(res.data)
            })
            .catch(res => {
                errorfn && errorfn(res.data)
            })
    },
    editRechargeType: function (data, successfn, errorfn) {
        return axios.get(apiList.system.editRechargeUrl, data)
            .then(res => {
                successfn && successfn(res.data)
            })
            .catch(res => {
                errorfn && errorfn(res.data)
            })
    },
    getWorkerData: function (data, successfn, errorfn) {
        return axios.get(apiList.system.getWorkerList, data)
            .then(res => {
                successfn && successfn(res.data)
            })
            .catch(res => {
                errorfn && errorfn(res.data)
            })
    },
    addWorkerData: function (data, successfn, errorfn) {
        return axios.get(apiList.system.addWorker, data)
            .then(res => {
                successfn && successfn(res.data)
            })
            .catch(res => {
                errorfn && errorfn(res.data)
            })
    },
    editWorkerData: function (data, successfn, errorfn) {
        return axios.get(apiList.system.editWorker, data)
            .then(res => {
                successfn && successfn(res.data)
            })
            .catch(res => {
                errorfn && errorfn(res.data)
            })
    },
    getIncomeList: function (data, successfn, errorfn) {
        return axios.get(apiList.system.getIncomeList, data)
            .then(res => {
                successfn && successfn(res.data)
            })
            .catch(res => {
                errorfn && errorfn(res.data)
            })
    },
    getIncomeDetail: function (data, successfn, errorfn) {
        return axios.get(apiList.system.getIncomeDetail, data)
            .then(res => {
                successfn && successfn(res.data)
            })
            .catch(res => {
                errorfn && errorfn(res.data)
            })
    },
    sysUserList: function (data, successfn, errorfn) {
        return axios.get(apiList.system.sysUserListUrl, data)
            .then(res => {
                successfn && successfn(res.data)
            })
            .catch(res => {
                errorfn && errorfn(res.data)
            })
    }
}
