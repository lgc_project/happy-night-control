import axios from 'axios'
import apiList from './resource.js'

export default {
    // 获取登录信息
    getInfo: function (successfn, errorfn) {
        return axios.get(apiList.account.info)
            .then(res => {
                successfn && successfn(res.data)
            })
            .catch(res => {
                errorfn && errorfn(res.data)
            })
    },
    // 用户登录
    postLogin: function (data, successfn, errorfn) {
        return axios.post(apiList.account.login + '?' + data)
            .then(res => {
                successfn && successfn(res.data)
            })
            .catch(res => {
                errorfn && errorfn(res.data)
            })
    },
    // 用户退出登录
    postLoginout: function (data, successfn, errorfn) {
        return axios.post(apiList.account.logout + '?' + data)
            .then(res => {
                successfn && successfn(res.data)
            })
            .catch(res => {
                errorfn && errorfn(res.data)
            })
    }
}
