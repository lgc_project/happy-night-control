import axios from 'axios'
import apiList from './resource.js'

export default {
    // 获取举报信息
    getReportData: function (data, successfn, errorfn) {
        return axios.get(apiList.report.list, data)
            .then(res => {
                successfn && successfn(res.data)
            })
            .catch(res => {
                errorfn && errorfn(res.data)
            })
    }
}
