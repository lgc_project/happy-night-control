import axios from 'axios'
import apiList from './resource.js'

export default {
    gethandleList: function (data, successfn, errorfn) {
        return axios.get(apiList.withdraw.list, data)
            .then(res => {
                successfn && successfn(res.data)
            })
            .catch(res => {
                errorfn && errorfn(res.data)
            })
    },
    handleData: function (data, successfn, errorfn) {
        return axios.get(apiList.withdraw.handle, data)
            .then(res => {
                successfn && successfn(res.data)
            })
            .catch(res => {
                errorfn && errorfn(res.data)
            })
    },
    detailData: function (data, successfn, errorfn) {
        return axios.get(apiList.withdraw.detail, data)
            .then(res => {
                successfn && successfn(res.data)
            })
            .catch(res => {
                errorfn && errorfn(res.data)
            })
    }
}
