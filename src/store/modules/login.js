import { writeData, clearData } from './../../lib/config'
import Router from './../../router'
import apiLogin from './../../api/login.js'

const state = {
    userName: '',
    // 权限
    authority: {},
    // 是否登出
    isSignOut: false
}

const getters = {
    userName: (state) => state.userName,
    authority: (state) => state.authority,
    isSignOut: (state) => state.isSignOut,
    userStore: (state) => state.user
}

const actions = {
    handleLogin ({ commit, dispatch }, data) {
        // 获取权限
        dispatch('getAuthority', data.authority)
        // // 设置用户名
        dispatch('saveUserName', data.userName)
        // // 储存history
        writeData({ module: 'account', data: data.account })
    },
    saveUserName ({ commit }, userName) {
        commit('SAVE_USER_NAME', userName)
    },
    signOut ({ commit }) {
        clearData({ module: 'account' })
        clearData({ module: 'authority' })
        commit('SIGNOUT')
    },
    // 获取权限
    getAuthority ({ commit }, authority) {
        // 设置权限
        commit('menuList', JSON.parse(authority))
    },
    // 登录接口，获取用户信息
    postLogin ({ commit }, data) {
        apiLogin.postLogin(data, res => {
            if (res.code === 200) {
                let data = res.data
                commit('SAVE_AUTHORITYDATA', data.authority)
                commit('SAVE_USER_NAME', data.userName)
                Router.push({ name: 'readme' })
            }
        })
    }
}

const mutations = {
    // 保存权限数据
    SAVE_AUTHORITYDATA (state, data) {
        state.authority = data
        // let menuList = [{
        //     title: '用户列表',
        //     index: 'user',
        //     childList: [{
        //         text: '男用户列表',
        //         index: 'user.man'
        //     }, {
        //         text: '女用户列表',
        //         index: 'user.woman'
        //     }, {
        //         text: '女性认证列表',
        //         index: 'user.identity'
        //     }, {
        //         text: '用户设备码列表',
        //         index: 'user.devicecode'
        //     }]
        // }, {
        //     title: '广播',
        //     index: 'broadCast',
        //     childList: [{
        //         text: '男士广播',
        //         index: 'broadCast.man'
        //     }, {
        //         text: '女士广播',
        //         index: 'broadCast.woman'
        //     }]
        // }, {
        //     title: '邀请码',
        //     index: 'code',
        //     childList: [{
        //         text: '新用户申请',
        //         index: 'code.new'
        //     }, {
        //         text: '老用户申请',
        //         index: 'code.old'
        //     }]
        // }, {
        //     title: '举报',
        //     index: 'report',
        //     childList: [{
        //         text: '男士举报',
        //         index: 'report.man'
        //     }, {
        //         text: '女士举报',
        //         index: 'report.woman'
        //     }]
        // }, {
        //     title: '评价',
        //     index: 'evaluate',
        //     childList: [{
        //         text: '男士评价',
        //         index: 'evaluate.man'
        //     }, {
        //         text: '女士评价',
        //         index: 'evaluate.woman'
        //     }]
        // }, {
        //     title: '提现',
        //     index: 'withdrawCash',
        //     childList: [{
        //         text: '待处理提现',
        //         index: 'withdrawCash.unhandle'
        //     }, {
        //         text: '已处理提现',
        //         index: 'withdrawCash.handle'
        //     }]
        // }, {
        //     title: '系统设置',
        //     index: 'system',
        //     childList: [{
        //         text: '系统参数',
        //         index: 'system.system'
        //     }, {
        //         text: '会员套餐',
        //         index: 'system.member'
        //     }, {
        //         text: '系统消息',
        //         index: 'system.message'
        //     }, {
        //         text: '收入记录',
        //         index: 'system.income'
        //     }, {
        //         text: '客服列表',
        //         index: 'system.worker'
        //     }]
        // }]
    },
    // 登出
    SIGNOUT (state) {
        state.isSignOut = true
    },
    // 保存用户名
    SAVE_USER_NAME (state, userName) {
        state.userName = userName
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}
