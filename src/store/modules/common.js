import Vue from 'vue'
import Router from './../../router'
import breadcrumbLists from './../../lib/breadcrumbLists'
import apiCommon from './../../api/common.js'
import apiLogin from './../../api/login.js'
// import apiDetection from './../../api/monitor.js'
import ElementUI from 'element-ui'
// import { Notification } from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

const state = {
    nav: '',
    breadcrumb: [],
    configure: {},
    getConfigureNum: 0,
    userInfo: {},
    setTime: ''
}

const getters = {
    sidebarNav: (state) => state.nav,
    breadcrumbData: (state) => state.breadcrumb,
    configure: (state) => state.configure,
    userInfo: (state) => state.userInfo,
    setTime: (state) => state.setTime
}

const actions = {
    // 改变左侧导航选中
    changeNav ({ commit }, data) {
        commit('SET_NAV', data)
    },
    // 定时检测是否还有未处理的数据
    checkMsg ({ commit }, data) {
        commit('SET_TIMEOUT', data)
    },
    // 面包屑数据
    breadcrumbHandle ({ commit }, name) {
        let items = []
        if (name) {
            const split = name.split('.')
            items = breadcrumbLists[split[0]][name]
        }
        commit('SET_BREADCRUMB_DATA', items)
    },
    // 获取配置
    getConfig ({ state, commit, dispatch }) {
        if (state.getConfigureNum === 2) {
            ElementUI.Message.error('获取基础配置失败，请重新登录')
            return false
        }
        apiCommon.getConfig(res => {
            if (res.code === 0) {
                commit('SET_CONFIG', res.data)
                dispatch('getDetection')
            } else {
                const num = state.getConfigureNum + 1
                // 设置失败获取次数
                commit('SET_GET_CONFIGURE_NUM', num)
                dispatch('getConfig')
            }
        })
    },
    // 监测用户信息，是否登录
    postLoginInfo ({ commit, dispatch }) {
        apiLogin.getInfo(res => {
            if (res.code === 0) {
                commit('SET_USER_INFOS', res.data)
                Router.push({ name: 'readme' })
                dispatch('getConfig')
            } else {
                clearTimeout(state.setTime)
                Router.push({ path: '/login' })
            }
        })
    },
    // 退出登录
    postLogout ({ commit }) {
        apiLogin.postLoginout(res => {
            if (res.code === 0) {
                clearTimeout(state.setTime)
                commit('SET_USER_INFOS', '')
                Router.push({ path: '/login' })
            }
        })
    },
    // 定时检测是否还有未处理的数据
    getDetection ({ commit, dispatch }) {
        // apiDetection.getDetection(res => {
        //     commit('SET_TIMEOUT', setTimeout(function () {
        //         // console.log(state.setTime)
        //         dispatch('getDetection')
        //     }, 60000))
        //     if (res.code === 0) {
        //         ElementUI.Notification({
        //             title: '提醒',
        //             message: res.data,
        //             type: 'warning',
        //             position: 'bottom-right',
        //             offset: 80,
        //             showClose: false
        //         })
        //     }
        // })
    }
}

const mutations = {
    SET_NAV (state, data) {
        state.nav = data
    },
    SET_TIMEOUT (state, data) {
        state.setTime = data
    },
    SET_BREADCRUMB_DATA (state, data) {
        Vue.set(state, 'breadcrumb', data)
    },
    SET_CONFIG (state, data) {
        Vue.set(state, 'configure', data)
    },
    SET_GET_CONFIGURE_NUM (state, num) {
        Vue.set(state, 'getConfigureNum', num)
    },
    SET_USER_INFOS (state, data) {
        Vue.set(state, 'userInfo', data)
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}
